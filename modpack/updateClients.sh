#!/bin/bash

rsyncMinecraft() {

location=$1

echo "--updating mods--"
rsync -ravz --delete --progress mods/ $location/mods
echo "--updating configs--"
rsync --ignore-existing -ravz --progress config/ $location/config

}

#update minecraft server
#echo "--uploading server mods--"
#rsync -avz mods/ nac:mohist/mods
#echo "--uploading server plugins--"
#rsync -avz plugins/ nac:mohist/plugins

#rsync -avz --delete --partial --progress plugins/*.jar nac:mohist/plugins
#rsync -avz --delete --partial --progress mods/* nac:mohist/mods
#rsync -avz --delete --partial --progress config/* nac:mohist/config

#update my local minecraft
rsyncMinecraft "/home/mmance/.atlauncher/instances/KomputerNerdsSurvival1122"
scp ../mods4me/ "/home/mmance/.atlauncher/instances/KomputerNerdsSurvival1122/mods"

#update sara's minecraft
rsyncMinecraft "sriser@sriser:.atlauncher/instances/KomputerNerdsSurvival1122"

echo "this script does not delete anything, it will have to be done manually."
