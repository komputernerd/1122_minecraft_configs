#!/bin/bash

#recover latest server configs
rsync -avz --progress nac:mohist/config/ config
rsync -avz --exclude 'dynmap' nac:mohist/plugins/ plugins
rsync -avz --delete nac:mohist/mods/ mods

